module www.velocidex.com/golang/vtypes

go 1.14

require (
	github.com/Velocidex/ordereddict v0.0.0-20200723153557-9460a6764ab8
	github.com/alecthomas/participle v0.7.1 // indirect
	github.com/alecthomas/repr v0.0.0-20201120212035-bb82daffcca2 // indirect
	github.com/davecgh/go-spew v1.1.1
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0
	github.com/sebdah/goldie v0.0.0-20180424091453-8784dd1ab561
	github.com/stretchr/testify v1.4.0
	golang.org/x/text v0.3.4 // indirect
	www.velocidex.com/golang/vfilter v0.0.0-20201229033732-028282d980f6
)

// replace www.velocidex.com/golang/vfilter => /home/mic/projects/vfilter
